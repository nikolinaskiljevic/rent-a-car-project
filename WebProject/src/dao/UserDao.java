package dao;

import java.util.ArrayList;

import model.User;

public class UserDao {
	private ArrayList<User> users;

	public UserDao() {
		users = new ArrayList<User>();
		User temp = new User(0, "username", "password", "Ime", "Prezime");
		users.add(temp);
	}
	
	private int newId() {
		int id = 0;
		for(User u : users) {
			if(u.getId() > id) {
				id = u.getId();
			}
		}
		return id + 1;
	}
	
	public User create(User u) {
		u.setId(newId());
		users.add(u);
		return u;
	}
	
	
	public ArrayList<User> getAll(){
		return new ArrayList<User>(users);
	}
}
