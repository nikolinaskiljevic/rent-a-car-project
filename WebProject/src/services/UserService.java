package services;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import dao.UserDao;
import model.User;

@Path("/users")
public class UserService {
	@Context
	ServletContext ctx;
	
	public UserService() {
	}

	@PostConstruct
	public void init() {
	    if (ctx.getAttribute("userDao") == null) {
	        ctx.setAttribute("userDao", new UserDao());
	    }
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers() {
        UserDao dao = (UserDao) ctx.getAttribute("userDao");
        return dao.getAll();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User createBook(User u) {
    	UserDao dao = (UserDao) ctx.getAttribute("userDao");
    	User user = dao.create(u);
        return user;
    }
}
