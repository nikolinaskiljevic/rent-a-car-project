Vue.component("main-page", {
	data: function() {
		return {
			users: null
		}
	},

	template: ` 
	<div>
		<h3>Users</h3>

		<table border="1" id="tabela">
			<thead>
				<tr bgcolor="lightgrey">
					<th>Id</th>
					<th>Username</th>
					<th>Password</th>
					<th>Name</th>
					<th>Surname</th>
				</tr>
			</thead>

			<tr v-for="u in users">
				<td>{{u.id }}</td>
				<td>{{u.username }}</td>
				<td>{{u.password}}</td>
  				<td>{{u.name}}</td>
 				<td>{{u.surname}}</td>
			</tr>
		</table>
	</div>	  
`
	,
	mounted() {
		axios.get('rest/users')
			.then(response => {
				this.users = response.data
			})
			

	},
	methods: {
		
	}
});
